﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace TaskPlanner.CRUD
{
    class CrudProject
    {
       private PlannerBaseEntities Context;
        public CrudProject()
        {
            Context=new PlannerBaseEntities();
        }

        /// <summary>
        /// Получить все проекты
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public IEnumerable<Project> GetAll()
        {
            return Context.Projects;
        }

        /// <summary>
        /// Получить проект по Id
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public Project Get(Guid guid)
        {
            return Context.Projects.FirstOrDefault(l => l.Id == guid);
        }

        /// <summary>
        /// Добавить проект
        /// </summary>
        /// <param name="record"></param>
         public bool Add(Project record)
         {
             if (Context.Projects.FirstOrDefault(u => u.Name == record.Name) != null)
             {
                 return false;
             }
             Context.Projects.Add(record);
             Context.SaveChanges();
             return true;
         }

        /// <summary>
        /// Удалить проект по Id
        /// </summary>
        /// <param name="guid"></param>
        public void Delete(Guid guid)
        {
            var list = Context.Projects;
            var taskAction = new CrudTask();
            var tasks = taskAction.GetAll().Where(t => t.ProjectId == guid).ToArray();

            foreach (var task in tasks)
            {
                taskAction.Delete(task.Id);
            }

            Context.Projects.Remove(list.FirstOrDefault(r => r.Id == guid));
            Context.SaveChanges();
        }

        /// <summary>
        /// Обновить проект
        /// </summary>
        /// <param name="record"></param>
        public void Update(Project record)
        {
            Context.Projects.AddOrUpdate(record);
            Context.SaveChanges();
        }
    }
}
