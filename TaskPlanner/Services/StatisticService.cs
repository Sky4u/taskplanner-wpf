﻿using System;
using System.Linq;
using TaskPlanner.Model;

namespace TaskPlanner.Services
{
    internal class StatisticService
    {
        public static ProjectStatistic GetProjectStat(Guid projectId)
        {
            var tasks = new CRUD.CrudTask().GetAll().Where(t => t.ProjectId == projectId);
            var users = tasks.GroupBy(t => t.UserId);

            return new ProjectStatistic()
            {
                TaskCount = tasks.Count(),
                UserCount = users.Count(),
                CompletedTasks = tasks.Where(t => t.Status == 2).Count(),
                NotStartedTasks = tasks.Where(t => t.Status == 0).Count(),
                TasksInProgress = tasks.Where(t => (t.Status == 4) || (t.Status == 1)).Count(),
                OverdueTasks = tasks.Where(t => t.Status == 3).Count(),
                CompletionPercentage = string.Format("{0:f2}", tasks.Where(t => t.Status == 2).Count() * 100.0 / tasks.Count()) + "%",
            };
        }
    }
}
