﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.Model;

namespace TaskPlanner.Services
{
    class AdminTableUserService
    {
        public static IEnumerable<AdminTableUser> GetUserTable(Guid id, IEnumerable<User> users)
        {
            //var adminTasks = tasks.Where(t => t.UserId == id); //нельзя показывать все таски, нужно выбрать только те что может видеть этот админ

            return
                users.Select(user => new AdminTableUser()
                {
                    Id = user.Id,
                    Login = user.Login,
                    Name = user.Name,
                    Password = user.Password,
                    StringRights = (user.Rights == 1) ? "Администратор" : "Пользователь",
                    Surname = user.Surname,
                    Rights = user.Rights
                }
            );
        }
    }
}
