﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.CRUD;
using TaskPlanner.Model;


namespace TaskPlanner.Services
{
    class AdminTableProjectService
    {
        public static IEnumerable<AdminTableProject> GetProjectTable(Guid id, IEnumerable<Project> projects)
        {
            //var adminTasks = tasks.Where(t => t.UserId == id); //нельзя показывать все таски, нужно выбрать только те что может видеть этот админ
            var users = new CrudUser().GetAll();
            return
                from user in users
                join project in projects on user.Id equals project.OwnerId
                select new AdminTableProject()
                {
                    ProjectId = project.Id,
                    ProjectName = project.Name,
                    UserFIO = user.Name + " " + user.Surname,
                    ProjectInfo = project.Info
                };
        }
    }
}
