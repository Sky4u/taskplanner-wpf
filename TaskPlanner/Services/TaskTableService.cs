﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskPlanner.CRUD;
using TaskPlanner.Types;

namespace TaskPlanner.Services
{
    class TaskTableService
    {
        protected static TimeSpan progressParser;
        public static string ProgressParser
        {
            get { return progressParser.ToString(); }
            set { progressParser = TimeSpan.Parse(value); }
        }

        private static string stringFormer(string taskProgress)
        {
            ProgressParser = taskProgress;
            if (progressParser.Seconds > 0)
            {
                progressParser = progressParser.Add(new TimeSpan(0, 1, 0));
                progressParser = progressParser.Subtract(new TimeSpan(0, 0, progressParser.Seconds));
            }
            return ((progressParser.Hours).ToString() + " : " + string.Format("{0:d2}", progressParser.Minutes));
        }

        public static UserTask GetUserTaskTable(Guid taskId)
        {
            var statusArray = new string[] { "Не начата", "В процессе", "Завершена", "Просрочена", "Приостановлена" };
            var statusColorArray = new string[] { "White", "#FFFEC7", "#E9FFC7", "#FFE1C7", "#C7F3FF" };
            var task = new CrudTask().Get(taskId);
            var project = new CrudProject().Get(task.ProjectId);
            return new UserTask()
                {
                    TaskId = task.Id,
                    UserId = task.UserId,
                    ProjectId = project.Id,
                    TableNumber = task.Number.ToString(),
                    TaskName = task.Name,
                    TaskInfo = task.Info,
                    TableStatus = statusArray[task.Status],
                    TableCreated = task.Created.ToLongDateString(),
                    TableDeadline = task.Deadline.ToLongDateString(),
                    OwnerId = project.OwnerId,
                    TableProgress = stringFormer(task.Progress),
                    ProjectInfo = project.Info,
                    ProjectName = project.Name,
                    TableAccess = task.Access ? "Открытый" : "Закрытый",
                    TableTimeRating = stringFormer(task.TimeRating),
                    TablePlannedOn = task.PlannedOn.GetValueOrDefault().ToLongDateString(),
                    Id = task.Id,
                    Number = task.Number,
                    PlannedOn = task.PlannedOn,
                    Access = task.Access,
                    TimeRating = task.TimeRating,
                    Name = task.Name,
                    Deadline = task.Deadline,
                    TableColorStatus = statusColorArray[task.Status],
                    Info = task.Info,
                    Created = task.Created,
                    Progress = task.Progress,
                    Status = task.Status
                };
        }

        public static IEnumerable<UserTask> GetTaskTable(Guid id)
        {
            var statusArray = new string[] { "Не начата", "В процессе", "Завершена", "Просрочена", "Приостановлена" };
            var statusColorArray = new string[] { "White", "#FFFEC7", "#E9FFC7", "#FFE1C7", "#C7F3FF" };
            var tasks = new CrudTask().GetAll().Where(t => t.UserId == id);
            var projects = new CrudProject().GetAll();
            return
                from task in tasks
                join project in projects on task.ProjectId equals project.Id
                select new UserTask()
                {
                    TaskId = task.Id,
                    UserId = task.UserId,
                    ProjectId = project.Id,
                    TableNumber = task.Number.ToString(),
                    TaskName = task.Name,
                    TaskInfo = task.Info,
                    TableStatus = statusArray[task.Status],
                    TableCreated = task.Created.ToLongDateString(),
                    TableDeadline = task.Deadline.ToLongDateString(),
                    OwnerId = project.OwnerId,
                    TableProgress = stringFormer(task.Progress),
                    ProjectInfo = project.Info,
                    ProjectName = project.Name,
                    TableAccess = task.Access ? "Открытый" : "Закрытый",
                    TableTimeRating = stringFormer(task.TimeRating),
                    TablePlannedOn = task.PlannedOn.GetValueOrDefault().ToLongDateString(),
                    Id = task.Id,
                    Number = task.Number,
                    PlannedOn = task.PlannedOn,
                    Access = task.Access,
                    TimeRating = task.TimeRating,
                    Name = task.Name,
                    Deadline = task.Deadline,
                    TableColorStatus = statusColorArray[task.Status],
                    Info = task.Info,
                    Created = task.Created,
                    Progress = task.Progress,
                    Status = task.Status
                };
        }
    }
}
