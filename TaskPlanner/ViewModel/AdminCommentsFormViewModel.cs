﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using TaskPlanner.Model;
using TaskPlanner.Services;

namespace TaskPlanner.ViewModel
{
    class AdminCommentsFormViewModel : ViewModelBase
    {
        private string _comment;
        private string _commentError;
        AdminTableTask TaskInfo { get; set; }
        public ICommand ClickAddComment { get; set; }
        public User CurrentUser { get; set; }
        private ObservableCollection<TableComment> _comments;
        public ObservableCollection<TableComment> Comments
        {
            get { return _comments; }
            set
            {
                _comments = value;
                this.OnPropertyChanged("Comments");
            }
        }
        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                this.OnPropertyChanged("Comment");
            }
        }
        public string CommentError
        {
            get { return _commentError; }
            set { _commentError = value; OnPropertyChanged("CommentError"); }
        }
        public AdminCommentsFormViewModel(User currentUser, AdminTableTask taskInfo)
        {
            CurrentUser = currentUser;
            TaskInfo = taskInfo;
            ReloadComments();
            ClickAddComment = new Command(arg => AddComment());
        }
        private void AddComment()
        {
            if (Comment == null || Comment == "")
            {
                CommentError = "Нельзя отправить пустой комментарий";
                return;
            }
            if (Comment != null && Comment != "")
            {
                CommentError = "";
            }
            var repository = new CRUD.CrudComment();
            repository.Add(new Comment(CurrentUser.Id, TaskInfo.TaskId, Comment));
            ReloadComments();
            Comment = "";
        }
        private void ReloadComments()
        {
            Comments = new ObservableCollection<TableComment>(CommentService.GetProjectTable(TaskInfo.TaskId));
        }
    }
}
