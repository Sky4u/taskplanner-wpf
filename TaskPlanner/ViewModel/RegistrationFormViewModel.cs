﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using TaskPlanner.Properties;

namespace TaskPlanner.ViewModel
{
    class RegistrationFormViewModel : ViewModelBase
    {
        public User CurrentUser { get; set; }
        public ICommand ClickSave { get; set; }
        private User _dataUser;
        public string FormName { get; set; }
        private string _login;
        private string _password;
        private string _name;
        private string _surname;
        private string _nameStar;
        private string _nameError;
        private string _surnameStar;
        private string _surnameError;
        private string _loginStar;
        private string _loginError;
        private string _passwordStar;
        private string _passwordError;
        private int _rights;
        private bool _active;
        public string curPass;
        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }
        public int Rights
        {
            get { return _rights; }
            set { _rights = value; OnPropertyChanged("Rights"); }
        }
        public string Login
        {
            get { return _login; }
            set { _login = value; OnPropertyChanged("Login"); }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged("Password"); }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }
        public string Surname
        {
            get { return _surname; }
            set { _surname = value; OnPropertyChanged("Surname"); }
        }
        public string NameStar
        {
            get { return _nameStar; }
            set { _nameStar = value; OnPropertyChanged("NameStar"); }
        }
        public string NameError
        {
            get { return _nameError; }
            set { _nameError = value; OnPropertyChanged("NameError"); }
        }
        public string SurnameStar
        {
            get { return _surnameStar; }
            set { _surnameStar = value; OnPropertyChanged("SurnameStar"); }
        }
        public string SurnameError
        {
            get { return _surnameError; }
            set { _surnameError = value; OnPropertyChanged("SurnameError"); }
        }
        public string LoginStar
        {
            get { return _loginStar; }
            set { _loginStar = value; OnPropertyChanged("LoginStar"); }
        }
        public string LoginError
        {
            get { return _loginError; }
            set { _loginError = value; OnPropertyChanged("LoginError"); }
        }
        public string PasswordStar
        {
            get { return _passwordStar; }
            set { _passwordStar = value; OnPropertyChanged("PasswordStar"); }
        }
        public string PasswordError
        {
            get { return _passwordError; }
            set { _passwordError = value; OnPropertyChanged("PasswordError"); }
        }
        public RegistrationFormViewModel(User currentUser)
        {
            CurrentUser = currentUser;
            Rights = 0;
            ClickSave = new Command(arg => SaveUser());
            FormName = Resources.RegUser;
            Active = true;
        }
        public RegistrationFormViewModel(User currentUser, User dataUser)
            : this(currentUser)
        {
            _dataUser = dataUser;
            Name = dataUser.Name;
            Surname = dataUser.Surname;
            Rights = dataUser.Rights;
            Login = dataUser.Login;
            Password = null;
            curPass = dataUser.Password;
            Active = dataUser.IsActive;
            FormName = Resources.EditUser;
        }
        private void SaveUser()
        {
            int errorCount = 0, loginPassCheck = 0;
            if (Name == null || Name == "")
            {
                NameStar = "*";
                NameError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (Name != null && Name != "")
            {
                NameStar = "";
                NameError = "";
            }
            if (Surname == null || Surname == "")
            {
                SurnameStar = "*";
                SurnameError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (Surname != null && Surname != "")
            {
                SurnameStar = "";
                SurnameError = "";
            }
            if (Login == null || Login == "")
            {
                LoginStar = "*";
                LoginError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (Login != null && Login != "")
            {
                loginPassCheck = errorCount;
                int spacebarCheck = Login.IndexOf(" ");
                if (spacebarCheck != -1)
                {
                    LoginStar = "*";
                    LoginError = "Поле может содержать только ла-\nтинские буквы, цифры и символ \"__\"";
                    errorCount++;
                }
                else
                {
                    Regex checkExp = new Regex(@"[\w]");
                    Match matchCheck = checkExp.Match(Login);
                    int charCount = 0;
                    while (matchCheck.Success)
                    {
                        matchCheck = matchCheck.NextMatch();
                        charCount++;
                        if (charCount < Login.Length - 1)
                        {
                            if (matchCheck.Success == false)
                            {
                                LoginStar = "*";
                                LoginError = "Поле может содержать только ла-\nтинские буквы, цифры и символ \"__\"";
                                errorCount++;
                                break;
                            }
                        }
                    }
                }
            }
            if (loginPassCheck == errorCount && Login != null)
            {
                LoginStar = "";
                LoginError = "";
            }
            if (Password == null || Password == "")
            {
                PasswordStar = "*";
                PasswordError = "Поле не должно быть пустым";
                errorCount++;
            }
            if (Password != null && Password != "")
            {
                loginPassCheck = errorCount;
                int spacebarCheck = Password.IndexOf(" ");
                if (spacebarCheck != -1)
                {
                    PasswordStar = "*";
                    PasswordError = "Поле может содержать только ла-\nтинские буквы, цифры и символ \"__\"";
                    errorCount++;
                }
                else
                {
                    Regex checkExp = new Regex(@"[\w]");
                    Match matchCheck = checkExp.Match(Password);
                    int charCount = 0;
                    while (matchCheck.Success)
                    {
                        matchCheck = matchCheck.NextMatch();
                        charCount++;
                        if (charCount < Password.Length - 1)
                        {
                            if (matchCheck.Success == false)
                            {
                                PasswordStar = "*";
                                PasswordError = "Поле может содержать только ла-\nтинские буквы, цифры и символ \"__\"";
                                errorCount++;
                                break;
                            }
                        }
                    }
                }
            }
            if (loginPassCheck == errorCount && Password != null)
            {
                PasswordStar = "";
                PasswordError = "";
            }
            if (errorCount > 0)
                return;
            if (_dataUser != null)
            {
                _dataUser.Login = Login;
                _dataUser.Name = Name;
                if (Password == null)
                {
                    _dataUser.Password = curPass;
                }
                else
                {
                    MD5 md5 = new MD5CryptoServiceProvider();
                    byte[] checkSum1 = md5.ComputeHash(Encoding.UTF8.GetBytes(Password));
                    Password = BitConverter.ToString(checkSum1).Replace("-", String.Empty);
                    _dataUser.Password = Password;
                }
                _dataUser.Rights = (byte)Rights;
                _dataUser.Surname = Surname;
                _dataUser.IsActive = Active;
                new CRUD.CrudUser().Update(_dataUser);
            }
            else
            {
                new CRUD.CrudUser().Add(new User(Login, Password, Name, Surname, (byte)Rights));
            }
            CloseView = true;
        }
    }
}
