﻿using System;
using System.Windows.Input;

namespace TaskPlanner.ViewModel
{
    class SheduleteTaskFormViewModel: ViewModelBase
    {
        public User CurrentUser { get; set; }
        public Task CurrentTask { get; set; }
        private string _deadlineString { get; set; }
        public string DeadlineString
        {
            get { return _deadlineString; }
            set
            {
                _deadlineString = value;
                this.OnPropertyChanged("DeadlineString");
            }
        }
        public DateTime SheduleteDate { get; set; }
        public ICommand ClickShedulete { get; set; }
        public SheduleteTaskFormViewModel(User currentUser, Task currentTask)
        {
            CurrentUser = currentUser;
            CurrentTask = currentTask;
            SheduleteDate = DateTime.Now;
            DeadlineString = CurrentTask.Deadline.ToLongDateString();
            ClickShedulete = new Command(arg => Shedulete());
        }
        private void Shedulete()
        {
            var repository = new CRUD.CrudTask();
            var task = repository.Get(CurrentTask.Id);
            task.PlannedOn = SheduleteDate;
            repository.Update(task);
            CloseView = true;
        }
    }
}
