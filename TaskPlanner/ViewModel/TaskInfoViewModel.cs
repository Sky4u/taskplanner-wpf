﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using TaskPlanner.Model;
using TaskPlanner.Services;
using TaskPlanner.Types;

namespace TaskPlanner.ViewModel
{
    class TaskInfoViewModel : ViewModelBase
    {
        private string _comment;
        private string _commentError;
        public ICommand ClickAddComment { get; set; }
        public User CurrentUser { get; set; }
        public UserTask TaskInfo { get; set; }
        private ObservableCollection<TableComment> _comments;
        private bool _userNameVisibility;
        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged("UserName");
            }
        }
        public bool UserNameVisibility
        {
            get { return _userNameVisibility; }
            set
            {
                _userNameVisibility = value;
                OnPropertyChanged("UserNameVisibility");
            }
        }
        public ObservableCollection<TableComment> Comments
        {
            get { return _comments; }
            set
            {
                _comments = value;
                this.OnPropertyChanged("Comments");
            }
        }
        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                this.OnPropertyChanged("Comment");
            }
        }
        public string CommentError
        {
            get { return _commentError; }
            set { _commentError = value; OnPropertyChanged("CommentError"); }
        }
        public TaskInfoViewModel(User currentUser, UserTask taskInfo)
        {
            if (currentUser.Rights == 1)
                UserNameVisibility = true;
            else
                UserNameVisibility = false;
            CurrentUser = currentUser;
            TaskInfo = taskInfo;
            UserName = new CRUD.CrudUser().Get(TaskInfo.UserId).ToString();
            ReloadComments();
            ClickAddComment = new Command(arg => AddComment());
        }
        private void AddComment()
        {
            if (Comment == null || Comment == "")
            {
                CommentError = "Нельзя отправить пустой комментарий";
                return;
            }
            if (Comment != null && Comment != "")
            {
                CommentError = "";
            }
            var repository = new CRUD.CrudComment();
            repository.Add(new Comment(CurrentUser.Id, TaskInfo.TaskId, Comment));
            ReloadComments();
            Comment = "";
        }
        private void ReloadComments()
        {
            Comments = new ObservableCollection<TableComment>(CommentService.GetProjectTable(TaskInfo.TaskId));
        }
    }
}
