﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using TaskPlanner.CRUD;
using TaskPlanner.Properties;
using TaskPlanner.Types;
using TaskPlanner.View;
using TaskPlanner.ViewModel.Helpers;

namespace TaskPlanner.ViewModel
{

    class UserWindowViewModel : ViewModelBase
    {
        public User CurrentUser { get; set; }
        public ICommand ClickChangeUser { get; set; }
        public ICommand ClickTimer { get; set; }
        public ICommand ClickSearch { get; set; }
        public ICommand ClickDetails { get; set; }
        public ICommand ClickFinish { get; set; }
        public ICommand ClickShedulete { get; set; }
        public ICommand ClickStopTimer { get; set; }


        public ICommand ClickChangeProjectFilter { get; set; }
        private ICommand _closeUserWindowCommand;
        protected TimeSpan progressParser;
        private string _searchText;
        private ObservableCollection<UserTask> _userTasks;
        private ObservableCollection<UserTask> _scheduledTasks;
        private ListCollectionView _taskDataGroup;
        private UserTask _selectedTask;
        private string _timerLabel;
        private string _taskName;
        private UserTask _selectedPlannedTask;
        private TimeSpan _time;
        private bool _timerInfoVisibility;
        private DispatcherTimer _timer;
        private Guid _selectedTaskId;
        private Guid _selectedTaskIdTimerEnabled;
        private ObservableCollection<Project> _projectsFilter;
        private int _currentStatusFilter = 1;
        private bool _isFinishVisible;
        private bool _isStartVisible;
        private bool _isPlannedVisible;
        private string _timerString;
        private string _finishString;
        private ObservableCollection<String> _statusFilter;
        private Project _selectedProject;


        public UserTask SelectedPlannedTask
        {
            get { return _selectedPlannedTask; }
            set
            {
                _selectedPlannedTask = value;
                SelectedTask = value;
                OnPropertyChanged("TaskDataGroup");
            }
        }
        public ListCollectionView TaskDataGroup
        {
            get { return _taskDataGroup; }
            set
            {
                _taskDataGroup = value;
                OnPropertyChanged("TaskDataGroup");
            }
        }
        public bool TimerInfoVisibility
        {
            get { return _timerInfoVisibility; }
            set
            {
                _timerInfoVisibility = value;
                OnPropertyChanged("TimerInfoVisibility");
            }
        }
        public string ProgressParser
        {
            get { return progressParser.ToString(); }
            set { progressParser = TimeSpan.Parse(value); }
        }
        public Project SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                _selectedProject = value;
                this.OnPropertyChanged("SelectedProject");
                CurrentUser.lastProject = value.Id;
                if (value.Id != Guid.Empty) new CrudUser().Update(CurrentUser); //стоит ли сохранять в базу при переключении проэкта?
                ReloadUserTasks();
            }
        }

        private void FilterProjects()
        {
            if (_selectedProject != null && _selectedProject.Id != Guid.Empty)
            {
                UserTasks = new ObservableCollection<UserTask>(UserTasks.Where(t => t.ProjectId == _selectedProject.Id));
            }
        }
        public int CurrentStatusFilter
        {
            get { return _currentStatusFilter; }
            set
            {
                _currentStatusFilter = value;
                this.OnPropertyChanged("CurrentStatusFilter");
                ReloadUserTasks();
            }
        }
        public string TimerString
        {
            get { return _timerString; }
            set
            {
                _timerString = value;
                this.OnPropertyChanged("TimerString");
            }
        }
        public string FinishString
        {
            get { return _finishString; }
            set
            {
                _finishString = value;
                this.OnPropertyChanged("FinishString");
            }
        }
        public bool IsStartVisible
        {
            get { return _isStartVisible; }
            set
            {
                _isStartVisible = value;
                this.OnPropertyChanged("IsStartVisible");
            }
        }
        public bool IsPlannedVisible
        {
            get { return _isPlannedVisible; }
            set
            {
                _isPlannedVisible = value;
                this.OnPropertyChanged("IsPlannedVisible");
            }
        }
        public bool IsFinishVisible
        {
            get { return _isFinishVisible; }
            set
            {
                _isFinishVisible = value;
                this.OnPropertyChanged("IsFinishVisible");
            }
        }
        public ObservableCollection<Project> ProjectsFilter
        {
            get { return _projectsFilter; }
            set
            {
                _projectsFilter = value;
                this.OnPropertyChanged("ProjectsFilter");
            }
        }
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                this.OnPropertyChanged("SearchText");
            }
        }
        public UserTask SelectedTask
        {
            get { return _selectedTask; }
            set
            {
                _selectedTask = value;
                this.OnPropertyChanged("SelectedTask");
                if (_selectedTask == null)
                {
                    IsStartVisible = false;
                    IsFinishVisible = false;
                    IsPlannedVisible = false;
                    FinishString = Resources.Finished;
                    TimerString = Resources.Start;
                }
                else
                {
                    IsPlannedVisible = true;
                    IsStartVisible = true;
                    IsFinishVisible = true;
                    if (_selectedTask.Status == 1)
                    {
                        FinishString = Resources.Finished;
                        TimerString = Resources.Stop;
                    }
                    if (_selectedTask.Status == 0 || _selectedTask.Status == 4)
                        TimerString = Resources.Start;
                    if (_selectedTask.Status == 3)
                    {
                        if (_timer.IsEnabled == false)
                            TimerString = Resources.Start;
                        else
                            if (_selectedTaskId == SelectedTask.Id && _timer.IsEnabled == true)
                            {
                                FinishString = Resources.Finished;
                                TimerString = Resources.Stop;
                            }
                            else
                                if (_selectedTaskId != SelectedTask.Id && _timer.IsEnabled == true)
                                    TimerString = Resources.Start;
                    }
                    if (_selectedTask.Status == 2)
                        FinishString = Resources.Restore;
                    else
                        FinishString = Resources.Finished;
                    if (_selectedTask.Status == 2)
                    {

                        IsPlannedVisible = false;
                        IsStartVisible = false;
                    }
                }
            }
        }
        public ObservableCollection<UserTask> UserTasks
        {
            get { return _userTasks; }
            set
            {
                _userTasks = value;
                this.OnPropertyChanged("UserTasks");
            }
        }
        //private ObservableCollection<UserTask> _scheduledTasks;
        public ObservableCollection<UserTask> ScheduledTasks
        {
            get { return _scheduledTasks; }
            set
            {
                _scheduledTasks = value;
                if (value != null)
                {
                    TaskDataGroup = new ListCollectionView(value);
                    TaskDataGroup.GroupDescriptions.Add(new PropertyGroupDescription("TablePlannedOn"));
                }
                this.OnPropertyChanged("ScheduledTasks");
            }
        }
        public string TimerLabel
        {
            get { return _timerLabel; }
            set
            {
                _timerLabel = value;
                this.OnPropertyChanged("TimerLabel");
            }
        }
        public string TaskName
        {
            get { return _taskName; }
            set
            {
                _taskName = value;
                this.OnPropertyChanged("TaskName");
            }
        }

        public ObservableCollection<String> StatusFilter
        {
            get { return _statusFilter; }
            set
            {
                _statusFilter = value;
                this.OnPropertyChanged("StatusFilter");
            }
        }
        public ICommand CloseUserWindowCommand
        {
            get
            {
                if (_closeUserWindowCommand == null)
                {
                    _closeUserWindowCommand = new RelayCommand(param => this.ClosingUserWindow());
                }
                return _closeUserWindowCommand;
            }
        }
        public UserWindowViewModel(User currentUser)
        {
            SelectedTask = null;
            CurrentUser = currentUser;
            TaskName = Resources.NameTask;
            TimerLabel = Resources.Timer;
            StatusFilter = new ObservableCollection<string>() { Resources.All, Resources.Unfulfilled, Resources.Made };
            ReloadUserTasks();
            ReloadScheduledTasks();

            _timer = new DispatcherTimer();
            _timer.Tick += new EventHandler(Timer_Tick);
            _timer.Interval = new TimeSpan(0, 0, 1);

            ClickChangeUser = new Command(arg => ChangeUser());
            ClickTimer = new Command(arg => Timer());
            ClickSearch = new Command(arg => ReloadUserTasks());
            ClickFinish = new Command(arg => Finish());
            ClickShedulete = new Command(arg => Shedulete());
            ClickDetails = new Command(arg => Details());
            ClickStopTimer = new Command(arg => StopTimer());



            var tasks = new CrudTask().GetAll().Where(t => t.UserId == CurrentUser.Id);
            var projects = new CrudProject().GetAll();
            var data = from task in tasks
                       join project in projects on task.ProjectId equals project.Id
                       group project by project.Id into g
                       select new { Remainder = g.Key, Numbers = g }; ;

            ProjectsFilter = new ObservableCollection<Project>();
            var actionProject = new CRUD.CrudProject();
            foreach (var g in data)
            {
                var t = actionProject.Get(g.Remainder);
                ProjectsFilter.Add(t);
            }

            ProjectsFilter.Insert(0, new Project(Resources.All, "", Guid.Empty) { Id = Guid.Empty });
            TaskOverdueCheck();
        }

        private void Details()
        {
            var ti = new TaskInfo()
            {
                DataContext = new TaskInfoViewModel(CurrentUser, SelectedTask)
            };
            ti.ShowDialog();
        }

        private void TaskOverdueCheck()
        {
            CrudTask taskOverdueWorker = new CrudTask();
            List<Guid> overdueTasksGuidList = new List<Guid>();
            List<Guid> recoverTasksGuidList = new List<Guid>();
            List<Task> overdueTasksList = new List<Task>();
            List<Task> recoverTasksList = new List<Task>();
            foreach (var item in UserTasks)
            {
                if ((item.Status != 2) && (item.Status != 3) && (item.Deadline < DateTime.Now))
                    overdueTasksGuidList.Add(item.Id);
                else
                    if ((item.Status == 3) && (item.Deadline > DateTime.Now))
                        recoverTasksGuidList.Add(item.Id);
            }
            if (overdueTasksGuidList.Count != 0)
            {
                overdueTasksList = taskOverdueWorker.GetMany(overdueTasksGuidList);
                foreach (var item in overdueTasksList)
                    item.Status = 3;
                taskOverdueWorker.UpdateMany(overdueTasksList);
            }
            if (recoverTasksGuidList.Count != 0)
            {
                recoverTasksList = taskOverdueWorker.GetMany(recoverTasksGuidList);
                foreach (var item in recoverTasksList)
                    item.Status = 4;
                taskOverdueWorker.UpdateMany(recoverTasksList);
            }
            ReloadUserTasks();
        }
        private void ReloadScheduledTasks()
        {
            ScheduledTasks = new ObservableCollection<UserTask>(UserTasks.Where(u => u.TablePlannedOn != Convert.ToDateTime("1900,01,01").ToLongDateString() && u.TableStatus != "Завершена"));
        }
        private void ClosingUserWindow()
        {
            if (_timer.IsEnabled == true)
            {
                StopTimer();
            }
        }
        private void StartTimer()
        {

            if (_timer.IsEnabled == true)
            {
                _selectedTaskIdTimerEnabled = SelectedTask.TaskId;
                StopTimer();
                ReloadUserTasks();
                SelectedTask = UserTasks.FirstOrDefault(t => t.TaskId == _selectedTaskIdTimerEnabled);
            }
            CrudTask crudTask = new CrudTask();
            Task currentTask = new Task();
            currentTask = crudTask.Get(SelectedTask.TaskId);
            //if (currentTask.Status != 3)
            {
                if (currentTask.Status != 3)
                    currentTask.Status = 1;
                crudTask.Update(currentTask);
            }
            TaskName = currentTask.Name;

            ProgressParser = currentTask.Progress;
            _time = progressParser;
            TimerInfoVisibility = true;
            _timer.Start();
            _selectedTaskId = SelectedTask.TaskId;
            ReloadUserTasks();
            SelectedTask = UserTasks.FirstOrDefault(t => t.TaskId == _selectedTaskId);
        }
        private void StopTimer()
        {
            TimerInfoVisibility = false;
            CrudTask crudTask = new CrudTask();
            Task currentTask = new Task();
            _timer.Stop();
            currentTask = crudTask.Get(_selectedTaskId);
            progressParser = _time;
            currentTask.Progress = ProgressParser;
            if (currentTask.Status != 3)
            {
                currentTask.Status = 4;
            }
            crudTask.Update(currentTask);
            ReloadUserTasks();
            SelectedTask = UserTasks.FirstOrDefault(t => t.TaskId == _selectedTaskId);
        }
        private void Search()
        {
            if (SearchText != null)
            {
                UserTasks = new ObservableCollection<UserTask>(UserTasks.Where(t => t.TaskName.ToLower().Contains(SearchText.ToLower()) || t.Number.ToString().ToLower().Contains(SearchText.ToLower())).ToList());
            }
        }
        private void ChangeUser()
        {
            var au = new Authorization
            {
                DataContext = new AuthorizationViewModel()
            };
            au.Show();
            CloseView = true;
        }

        public void FilterStatus(int value)
        {
            if (value == 2) UserTasks = new ObservableCollection<UserTask>(UserTasks.Where(t => t.Status == 2));
            else if (value == 1) UserTasks = new ObservableCollection<UserTask>(UserTasks.Where(t => t.Status != 2));
        }
        private void ReloadUserTasks()
        {
            var userList = Services.TaskTableService.GetTaskTable(CurrentUser.Id);
            UserTasks = new ObservableCollection<UserTask>(userList);
            Search();
            FilterStatus(CurrentStatusFilter);
            FilterProjects();
        }

        private void Finish()
        {
            if (SelectedTask.TaskId != Guid.Empty)
            {
                if ((_timer.IsEnabled == true) && (SelectedTask.TaskId == _selectedTaskId))
                {
                    _selectedTaskIdTimerEnabled = SelectedTask.TaskId;
                    StopTimer();
                    ReloadUserTasks();
                    SelectedTask = UserTasks.FirstOrDefault(t => t.TaskId == _selectedTaskIdTimerEnabled);
                }
                CrudTask crudTask = new CrudTask();
                Task currentTask = new Task();
                currentTask = crudTask.Get(SelectedTask.TaskId);
                if (currentTask.Status != 2)
                {
                    currentTask.Status = 2;
                    crudTask.Update(currentTask);
                }
                else
                    if (currentTask.Status == 2)
                    {
                        currentTask.Status = 4;
                        crudTask.Update(currentTask);
                    }
            }
            ReloadUserTasks();
            ReloadScheduledTasks();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            _time = _time.Add(new TimeSpan(0, 0, 1));
            TimerLabel = _time.ToString();
        }

        private void Timer()
        {
            if (SelectedTask != null)
            {
                if (_timer.IsEnabled == true && _selectedTaskId != SelectedTask.Id && SelectedTask.Status == 3)
                    StartTimer();
                else
                    if ((_timer.IsEnabled == true && SelectedTask.Status == 3) || SelectedTask.Status == 1)
                        StopTimer();
                    else
                        if (SelectedTask.Status == 0 || SelectedTask.Status == 3 || SelectedTask.Status == 4)
                            StartTimer();
            }

        }

        private void Shedulete()
        {
            if (SelectedTask == null)
                return;
            var w = new SheduleteTaskForm
            {
                DataContext = new SheduleteTaskFormViewModel(CurrentUser, SelectedTask)
            };
            w.ShowDialog();
            ReloadUserTasks();
            ReloadScheduledTasks();
        }
    }
}
