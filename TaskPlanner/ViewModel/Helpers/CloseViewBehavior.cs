﻿using System.Windows;

namespace TaskPlanner.ViewModel
{
    public class CloseViewBehavior
    {
        // Регистрируем вложенное свойство
        public static readonly DependencyProperty CloseViewProperty =
            DependencyProperty.RegisterAttached("CloseView", typeof(bool), typeof(CloseViewBehavior),
            new PropertyMetadata(false, OnCloseViewChanged));

        public static bool GetCloseView(Window obj)
        {
            return (bool)obj.GetValue(CloseViewProperty);
        }

        // На самом деле вложенные свойства - это вовсе не свойства.
        // Когда мы пишем Grid.Row = "0" в xaml'е, это транслируется в такие вот статические ИмяКласса.SetИмяСвойства - Grid.SetRow например
        public static void SetCloseView(Window obj, bool value)
        {
            obj.SetValue(CloseViewProperty, value);
        }
        private static void OnCloseViewChanged(DependencyObject dpo, DependencyPropertyChangedEventArgs args)
        {
            Window win = dpo as Window;
            bool? close = (bool)args.NewValue;

            if (win != null)
            {
                // Если свойство устанавливается в истину - закрыть окно
                if (close==true)
                {
                    win.Close();
                }
            }
        }
    }
}
