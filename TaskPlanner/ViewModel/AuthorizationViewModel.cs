﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Input;
using TaskPlanner.CRUD;
using TaskPlanner.Properties;
using TaskPlanner.Services;
using TaskPlanner.View;

namespace TaskPlanner.ViewModel
{
    class AuthorizationViewModel : ViewModelBase
    {

        private string _login;
        private string _password;
        private string _loginStar;
        private string _loginError;
        private string _passwordStar;
        private string _passwordError;
        public ICommand ClickLogin { get; set; }
        public ICommand ClickClose { get; set; }
        public string Login
        {
            get { return _login; }
            set { _login = value; OnPropertyChanged("Login"); }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged("Password"); }
        }
        public string LoginStar
        {
            get { return _loginStar; }
            set { _loginStar = value; OnPropertyChanged("LoginStar"); }
        }
        public string LoginError
        {
            get { return _loginError; }
            set { _loginError = value; OnPropertyChanged("LoginError"); }
        }
        public string PasswordStar
        {
            get { return _passwordStar; }
            set { _passwordStar = value; OnPropertyChanged("PasswordStar"); }
        }
        public string PasswordError
        {
            get { return _passwordError; }
            set { _passwordError = value; OnPropertyChanged("PasswordError"); }
        }
        public AuthorizationViewModel()
        {
            ClickLogin = new Command(arg => GoLogin());
            ClickClose = new Command(arg => CloseProgram());
        }
        private void CloseProgram()
        {
            CloseView = true;
        }
        private void GoLogin()
        {
            bool errorLogin = false, errorPassword = false;
            var userCount = new CrudUser().GetAll().Count();

            if (Login == null || Login == "")
            {
                LoginStar = "*";
                LoginError = "Поле не должно быть пустым";
                errorLogin = true;
            }
            if (Login != null && Login != "")
            {
                LoginStar = "";
                LoginError = "";
            }
            if (Password == null || Password == "")
            {
                PasswordStar = "*";
                PasswordError = "Поле не должно быть пустым";
                errorPassword = true;
            }
            if (Password != null && Login != "")
            {
                PasswordStar = "";
                PasswordError = "";
            }

            if (userCount < 1)
                DataFactory.MakeRecords(5);

            var userAction = new CrudUser();
            var list = userAction.GetAll();


            if (errorLogin == false)
            {
                var userCheck = list.FirstOrDefault(u => (u.Login.ToLower() == Login.ToLower()));

                if (userCheck == null)
                {
                    LoginStar = "*";
                    LoginError = "Логин введён не верно";
                    errorLogin = true;
                }
                if (userCheck != null)
                {
                    LoginStar = "";
                    LoginError = "";
                }
            }

            if (errorPassword == false)
            {
                string strCheck = Password;
                MD5 md5Check = new MD5CryptoServiceProvider();
                byte[] checkSumsCheck = md5Check.ComputeHash(Encoding.UTF8.GetBytes(strCheck));
                strCheck = BitConverter.ToString(checkSumsCheck).Replace("-", String.Empty);
                var userCheck = list.FirstOrDefault(u => (u.Password == strCheck));

                if (userCheck == null)
                {
                    PasswordStar = "*";
                    PasswordError = "Пароль введён не верно";
                    errorPassword = true;
                }
                if (userCheck != null)
                {
                    PasswordStar = "";
                    PasswordError = "";
                }
            }
            if (errorLogin == true || errorPassword == true)
                return;

            string str = Password;
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] checkSums = md5.ComputeHash(Encoding.UTF8.GetBytes(str));
            str = BitConverter.ToString(checkSums).Replace("-", String.Empty);

            var user = list.FirstOrDefault(u => (u.Login.ToLower() == Login.ToLower()) && (u.Password == str));

            if (user.IsActive == false)
            {
                MessageBox.Show(Resources.VM3);
                return;
            }

            if (user.Rights == 0)
            {
                var w = new UserWindow
                {
                    DataContext = new UserWindowViewModel(user)
                };
                ((UserWindowViewModel)w.DataContext).SelectedProject =
                    ((UserWindowViewModel)w.DataContext).ProjectsFilter.FirstOrDefault(p => p.Id == user.lastProject);
                w.Show();
            }
            else
            {
                var w = new AdminWindow
                {
                    DataContext = new AdminWindowViewModel(user)
                };
                ((AdminWindowViewModel)w.DataContext).SelectedProjectFilter =
                     ((AdminWindowViewModel)w.DataContext).ProjectsFilter.FirstOrDefault(p => p.Id == user.lastProject);

                w.Show();
            }
            CloseView = true;
        }
    }
}
