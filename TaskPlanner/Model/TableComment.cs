﻿
namespace TaskPlanner.Model
{
    class TableComment:Comment
    {
        public string UserName { get; set; }
        public string UserRights { get; set; }
        public string DateString { get; set; }
    }
}
