﻿using System;

namespace TaskPlanner.Model
{
    class AdminTableProject
    {
        public Guid ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectInfo { get; set; }
        public string UserFIO { get; set; }
    }
}
