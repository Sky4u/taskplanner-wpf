﻿
namespace TaskPlanner.Model
{
    class AdminTableUser : User
    {
        public string StringRights { get; set; } // 0 - user, 1 - admin
    }
}
