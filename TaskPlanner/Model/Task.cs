﻿using System;

namespace TaskPlanner
{
  partial  class Task
    {
      //public Task() { }
      public Task(Guid userId, Guid projectId, int number, string name, string info, string progress,string timeRating, DateTime deadline = default(DateTime), bool access = false)
        {
            Id = Guid.NewGuid();
            UserId = userId;
            ProjectId = projectId;
            Number = number;
            Name = name;
            Info = info;
            Created = DateTime.Today;
            Progress = progress;
            Deadline = deadline;
            Access = access;
            TimeRating = timeRating;
            PlannedOn = new DateTime(1900,01,01);
        }
    }
}
